import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder, FormControl, Validators } from '@angular/forms';
import { config } from '../_helpers/global';
import { PatientServiceService } from '../_services/patient-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  PatientForm : FormGroup;
  patients: any;
  submitted = false;
  loading = false;
  submiterror = false;
  formerror = false;
  success = false;
  existing = false;

  constructor(private fb: FormBuilder, private ps : PatientServiceService) { }

  ngOnInit(): void {
    this.PatientForm = this.fb.group({
      name : ['', Validators.required],
      dob : ['', Validators.required],
      gender : ['', Validators.required],
      service : ['', Validators.required],
      comments : ['', Validators.required]
    });
    this.getPatients();
  }

  submitPatient() {
    this.submitted = true;
    if (this.PatientForm.invalid) {
      return;
    } else {
      let patientdata = this.PatientForm.value;
      this.loading = true;
      console.log(patientdata);
      this.ps.submitPatient(patientdata).subscribe( data => {
        this.loading = false;
        this.submitted = false;
        if (data == 1) {
          this.success = true;
          this.submiterror = false;
          this.PatientForm.reset();
          this.getPatients();
        } else {
          this.submiterror = true;
        }
      }, error => {
        console.log(error);
      })
    }
  }

  getPatients () {
    this.ps.getPatients().subscribe( data=> {
      this.patients = data;
      console.log(this.patients);
    }, error=> {
      console.error();
      
    })
  }
}
