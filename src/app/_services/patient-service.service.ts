import { Injectable } from '@angular/core';
import { config } from '../_helpers/global';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatientServiceService {

  constructor( private http : HttpClient) { }

  submitPatient (data) {
    return this.http.post<any>(`${config.apiUrl}/post_patient`, {data: data});
  }

  getPatients () {    
    return this.http.get<any>(`${config.apiUrl}/get_patients`);
  }
}
